The Drutopia distribution will be the flagship libre software product of the [Drutopia](http://drutopia.org/) initiative.

# Contributing

Please take note of the [technical guide](https://gitlab.com/drutopia/drutopia-distribution/wikis/technical-guide) and [criteria for extensions](https://gitlab.com/drutopia/drutopia-distribution/wikis/extension-criteria-and-candidates) when contributing.

# Documentation

You can find [meeting notes](https://gitlab.com/drutopia/drutopia-distribution/wikis/meeting-notes) and other important documents in the [project's wiki](https://gitlab.com/drutopia/drutopia-distribution/wikis/home).